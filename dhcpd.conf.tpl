option domain-name DHCPD_DOMAIN_NAME;
option domain-name-servers DHCPD_DNS_SERVERS;

default-lease-time DHCPD_LEASE_TIME;
max-lease-time DHCPD_MAX_LEASE_TIME;
ddns-update-style none;
authoritative;

subnet DHCPD_SUBNET netmask DHCPD_NETMASK {
    range DHCPD_RANGE;
    option routers DHCPD_GATEWAY;
}

host roomba {
    hardware ethernet 80:C5:F2:BB:98:9C;
    fixed-address 192.168.1.25;
}

host googlehome {
    hardware ethernet b0:2a:43:13:d7:ef;
    fixed-address 192.168.1.7;
}

host tradfri {
    hardware ethernet a0:c9:a0:d7:b4:e5;
    fixed-address 192.168.1.36;
}
