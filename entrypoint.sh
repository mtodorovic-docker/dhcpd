#!/bin/bash

set -xe 

DHCPD_USER=dhcpd

if [ ! -z "$LISTEN_ADDRESS" ]; then
    LISTEN_FLAG="-s $LISTEN_ADDRESS"
fi

sed -e "s:{{ DHCPD_LEASE_TIME }}:$DHCPD_LEASE_TIME:" \
    -e "s:{{ DHCPD_RANGE }}:$DHCPD_RANGE:" \
    -e "s:{{ DHCPD_SUBNET }}:$DHCPD_SUBNET:" \
    -e "s:{{ DHCPD_DNS_SERVER }}:$DHCPD_DNS_SERVER:" \
    -e "s:{{ DHCPD_DOMAIN_NAME }}:$DHCPD_DOMAIN_NAME:" \
    -e "s:{{ DHCPD_MAX_LEASE_TIME }}:$DHCPD_MAX_LEASE_TIME:" \
    -e "s:{{ DHCPD_GATEWAY }}:$DHCPD_GATEWAY:" \
    -e "s:{{ DHCPD_NETMASK }}:$DHCPD_NETMASK:" \
    /root/dhcpd.conf.tpl > /etc/dhcp/dhcpd.conf

mkdir -p $DHCPD_LEASE_PATH
touch $DHCPD_LEASE_PATH/dhcpd.leases
chown $DHCPD_USER -R $DHCPD_LEASE_PATH
dhcpd -d -f -cf /etc/dhcp/dhcpd.conf -lf $DHCPD_LEASE_PATH/dhcpd.leases \
  -user $DHCPD_USER -group daemon $LISTEN_FLAG $DHCPD_INTERFACE &
