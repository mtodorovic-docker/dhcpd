FROM debian:stable-slim

LABEL org.label-schema.name=dhcpd \
    org.label-schema.vcs-url=https://gitlab.com/mtodorovic-docker/dhcpd

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      isc-dhcp-server && \
    apt-get -y autoremove && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/* && \
    useradd -ms /bin/bash dhcpd

ENV DHCPD_LEASE_TIME=3600 \
    DHCPD_RANGE="192.168.1.10 192.168.1.200" \
    DHCPD_DNS_SERVER="1.1.1.1" \
    DHCPD_MAX_LEASE_TIME=14400 \
    DHCPD_SUBNET=192.168.1.0 \
    DHCPD_GATEWAY=192.168.1.1 \
    DHCPD_NETMASK=255.255.255.0 \
    DHCPD_INTERFACE=eth0 \
    DHCPD_LEASE_PATH=/var/lib/dhcp \
    TZ=UTC

COPY entrypoint.sh /entrypoint.sh
COPY dhcpd.conf.tpl /root/dhcpd.conf.tpl
COPY dhcpd6.conf.tpl /root/dhcpd6.conf.tpl

EXPOSE 67/udp

ENTRYPOINT ["/entrypoint.sh"]
